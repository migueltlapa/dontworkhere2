from django.conf.urls import url
from .import views

urlpatterns=[
    url(r'^$',views.index,name='index'),
    url(r'^all/$',views.companies),
    url(r'^get/(?P<company_id>\d+)/$',views.company_one),

]