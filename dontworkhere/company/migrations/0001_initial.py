# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('title', models.TextField(max_length=254)),
                ('body', models.TextField()),
                ('likes', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('text', models.TextField()),
                ('article', models.ForeignKey(to='company.Article')),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=50, help_text='Max length 50')),
                ('logo', models.ImageField(null=True, upload_to='')),
                ('bussiness_type', models.CharField(max_length=20, help_text='Max length 20')),
                ('city', models.CharField(max_length=30, help_text='Max length 30')),
                ('domain', models.URLField(help_text='Max length 200')),
                ('top_general', models.PositiveSmallIntegerField()),
                ('rating', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Complaint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('text_complaint', models.TextField()),
                ('created_time', models.DateTimeField(verbose_name='Created Time', null=True, auto_now_add=True)),
                ('numbers', models.IntegerField(null=True, default=1)),
            ],
        ),
        migrations.CreateModel(
            name='Complaint_Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=50, help_text='Max length 50')),
            ],
        ),
        migrations.AddField(
            model_name='complaint',
            name='categories',
            field=models.ManyToManyField(to='company.Complaint_Category'),
        ),
        migrations.AddField(
            model_name='complaint',
            name='company',
            field=models.ForeignKey(related_name='company', to='company.Company'),
        ),
    ]
