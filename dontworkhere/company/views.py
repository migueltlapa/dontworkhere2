from django.shortcuts import render
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse,Http404
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views import View
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.shortcuts import render_to_response

from django.db.models import Count

from . import models
import json
import datetime

# Create your views here.

class BaseView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'hello.html', {
                        "name":"hola base view"
                        })

    def post(self, request):
        return render(request, 'index.html', {
                        "greet":"recibi el post"
                        })

    def update(self, request):
        return render(request, 'index.html', {

            "greet": "update"

        })

class HomeTemplateView(TemplateView):

    template_name = "hello.html"
    print("HomeTemasaplate")
    def get_context_data(self, **kwargs):
        context = super(HomeTemplateView, self).get_context_data(**kwargs)
        context["name"] = " HomeTemplateView"
        return context

class CompanyDetailView(DetailView):
    print("DetailView")
    model = models.Complaint

    template_name = 'detail.html'

class CompanyList(ListView):
    model = models.Complaint
    template_name= 'list.html'

    # def get_context_data(self, **kwargs):
    #     context = super(PersonList, self).get_context_data(**kwargs)
    #     context["object_list"] = models.Person.objects.older()
    #     return context

class CompanyCreate(CreateView):
    model = models.Complaint
    fields = '__all__'
    template_name = 'create.html'
    success_url = reverse_lazy('list')

class CompanyDelete(DeleteView):
    model = models.Complaint
    template_name = 'delete_confirm.html'
    success_url = reverse_lazy('list')


#updateview
class CompanyUpdate(UpdateView):
    model = models.Complaint
    fields = '__all__'
    template_name = 'create.html'
    success_url = reverse_lazy('list')

def index(request):
    categories = models.Categorycomplain.objects.all()
    data = {}
    if categories.count() >=1:
        for category in categories:
            data[category.name] = list(models.Complaint.objects.filter(categories__name=category.name).values('categories__name', 'company__name', 'company__id').annotate(Count('id')).order_by('categories__name', '-id__count')[:10])
    return render(request,'main.html',{'data':data, 'categories':list(categories)})
