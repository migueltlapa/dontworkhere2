from django.contrib import admin
from .models import Company,Categorycomplain,Complaint,Article,Comment
# Register your models here.
admin.site.register(Company)
admin.site.register(Categorycomplain)
admin.site.register(Complaint)
admin.site.register(Article)
admin.site.register(Comment)